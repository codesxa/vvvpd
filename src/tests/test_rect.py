import pytest
from project.rect import rect_sum


@pytest.fixture(scope='module')
def make_img():
    return [[30, 57, 80], [1, 0, 90], [5, 187, 905]]


@pytest.mark.parametrize('x1, y1, x2, y2, result',
                         [(0, 0, 2, 1, 280)])
def test_both_zero(make_img, x1, y1, x2, y2, result):
    assert rect_sum(make_img, x1, y1, x2, y2) == result


@pytest.mark.parametrize('x1, y1, x2, y2, result',
                         [(0, 1, 2, 2, 1319)])
def test_x_zero(make_img, x1, y1, x2, y2, result):
    assert rect_sum(make_img, x1, y1, x2, y2) == result


@pytest.mark.parametrize('x1, y1, x2, y2, result',
                         [(1, 0, 2, 2, 1188)])
def test_y_zero(make_img, x1, y1, x2, y2, result):
    assert rect_sum(make_img, x1, y1, x2, y2) == result


@pytest.mark.parametrize('x1, y1, x2, y2, result',
                         [(1, 1, 2, 2, 1182)])
def test_no_zero(make_img, x1, y1, x2, y2, result):
    assert rect_sum(make_img, x1, y1, x2, y2) == result


if __name__ == '__main__':
    pytest.main()
