import pytest
from project.integral import integral_view


def test_empty_list():
    assert integral_view([[]]) == [[]]


def test_one_el_list():
    assert integral_view([[5]]) == [[5]]


def test_row_list():
    assert integral_view([[7, 15, 9]]) == [[7, 22, 31]]


def test_column_list():
    assert integral_view([[12], [8], [9]]) == [[12], [20], [29]]


def test_normal_list():
    assert integral_view([[7, 5], [8, 13]]) == [[7, 12], [15, 33]]


if __name__ == '__main__':
    pytest.main()
