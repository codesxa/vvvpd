from project.integral import integral_view


def rect_sum(matrix: list, x1: int, y1: int, x2: int, y2: int) -> int:
    """
    На основе интегрального представления изображения функция считает сумму пискелей прямоугольника с заданными координ
    атами

    :param matrix: Представление изображения в виде матрицы (list)
    :param x1: Координата верхнего левого угла по оси x (int)
    :param y1: Координата верхнего левого угла по оси y (int)
    :param x2: Координата нижнего правого угла по оси x (int)
    :param y2: Коордианат нижнего правого угла по оси y (int)
    :return: Сумма пикселей прямоугольника с заданными координатами (int)
    """
    sum_mtr = integral_view(matrix)
    if x1 == 0 and y1 == 0:
        return sum_mtr[x2][y2]
    elif x1 == 0:
        return sum_mtr[x2][y2] - sum_mtr[x2][y1 - 1]
    elif y1 == 0:
        return sum_mtr[x2][y2] - sum_mtr[x1 - 1][y2]
    return sum_mtr[x2][y2] - sum_mtr[x2][y1 - 1] - sum_mtr[x1 - 1][y2] + sum_mtr[x1 - 1][y1 - 1]
