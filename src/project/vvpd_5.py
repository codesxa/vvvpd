def main():
    from matrix_random import get_matrix
    from integral import integral_view
    from rect import rect_sum

    image = get_matrix()
    print('Исходная матрица пикселей:')
    for row in image:
        print(*row)

    integral_img = integral_view(image)
    print('\nИнтегральное представление изображения: ')
    for row in integral_img:
        print(*row)

    x1, y1 = (int(i) for i in input('\nВведите через пробел коор-ты левого верхнего угла: ').split())
    x2, y2 = (int(i) for i in input('Введите через пробел коор-ты правого нижнего угла: ').split())
    rect = rect_sum(image, x1, y1, x2, y2)
    print(f'\nСумма пикселей прямоугольника: {rect}')


if __name__ == '__main__':
    main()
