def integral_view(matrix: list) -> list:
    """
    Функция получает на вход матрицу (представление изображения) и возвращает матрицу того же размера, которая является
    интегральным представлением изображения. Каждый элемент новой матрицы является суммой всех элементов исходной матри
    цы, которые расположены не ниже и не правее данного элемента

    :param matrix: Представление изображения в виде матрицы (list)
    :return: Интегральное представление изображения (list)
    """
    import copy
    integral_mtr = copy.deepcopy(matrix)
    row = len(matrix)
    column = len(matrix[0])
    for i in range(row):
        for j in range(column):
            if i == 0 and j == 0:
                continue
            if j != 0:
                integral_mtr[i][j] += integral_mtr[i][j - 1]
            if i != 0:
                integral_mtr[i][j] += integral_mtr[i - 1][j]
            if i != 0 and j != 0:
                integral_mtr[i][j] -= integral_mtr[i - 1][j - 1]
    return integral_mtr

