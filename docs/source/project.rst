project package
===============

Submodules
----------

project.integral module
-----------------------

.. automodule:: project.integral
   :members:
   :undoc-members:
   :show-inheritance:

project.matrix\_random module
-----------------------------

.. automodule:: project.matrix_random
   :members:
   :undoc-members:
   :show-inheritance:

project.rect module
-------------------

.. automodule:: project.rect
   :members:
   :undoc-members:
   :show-inheritance:

project.vvpd\_5 module
----------------------

.. automodule:: project.vvpd_5
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: project
   :members:
   :undoc-members:
   :show-inheritance:
