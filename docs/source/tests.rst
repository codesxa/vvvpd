tests package
=============

Submodules
----------

tests.test\_integral module
---------------------------

.. automodule:: tests.test_integral
   :members:
   :undoc-members:
   :show-inheritance:

tests.test\_rect module
-----------------------

.. automodule:: tests.test_rect
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tests
   :members:
   :undoc-members:
   :show-inheritance:
